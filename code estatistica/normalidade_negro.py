
import scipy.stats as stats
y = [5.3, 5.25, 6.27, 7.42, 6.9, 6.62, 28.71, 11.3, 14.98, 3.03, 11.8, 13.35, 11.1, 4.31, 6.85,
7.43, 5.8, 12.42, 13.76, 8.16, 7.23, 13.13, 10.65, 10.79, 8.99, 18.62, 5.36
]
shapiro_stat, shapiro_p_valor = stats.shapiro(y)
print("\n")
print("----------------------------------------------------------------------")
print(f"O valor da estatistica de shapiro-wilk = {shapiro_stat}")
print("----------------------------------------------------------------------")
print(f"O valor de P de shapiro-wilk = {shapiro_p_valor}")
print("----------------------------------------------------------------------")
if shapiro_p_valor > 0.01:
    print("com 95%\ de confiança, os dados são similares a uma distribuição normal")
else:
    print("com 95%\ de confiança, os dados não são similares a uma distribuição normal")
print("----------------------------------------------------------------------")
print("\n")