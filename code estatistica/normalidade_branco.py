
import scipy.stats as stats
y = [13.99,16.29,17.82,15.39,18.92,16.45,52.63,30.91,28.34,8.82,29.68,28.63,24.56,12.1,15.46,19.26,14.58,31.57,35.4,18.88,14.78,
25.73,34.76,36.55,20.51,43.06,14.66]
shapiro_stat, shapiro_p_valor = stats.shapiro(y)
print("\n")
print("----------------------------------------------------------------------")
print(f"O valor da estatistica de shapiro-wilk = {shapiro_stat}")
print("----------------------------------------------------------------------")
print(f"O valor de P de shapiro-wilk = {shapiro_p_valor}")
print("----------------------------------------------------------------------")
if shapiro_p_valor > 0.01:
    print("com 95%\ de confiança, os dados são similares a uma distribuição normal")
else:
    print("com 95%\ de confiança, os dados não são similares a uma distribuição normal")
print("----------------------------------------------------------------------")
print("\n")