import pandas as pd
import numpy as np
from scipy import stats
from scipy.stats import norm
import matplotlib.pyplot as plt

dados = pd.read_csv("G5-CORRELAÇÃO_leo_Lucas.csv")
x = dados.iloc[0:27,1]
y = dados.iloc[0:27,2]

stats.probplot(x, plot = plt)
stats.probplot(y, plot = plt)