#matplotlib inline
import pandas as pd
import numpy as np
import seaborn as sns
sns.set(style='whitegrid')

negro= [5.30, 5.25, 6.27, 7.42, 6.90, 6.62, 28.71, 11.33, 14.98, 3.03, 11.8, 13.35, 11.10, 4.31, 6.85, 7.43, 5.80, 12.42, 13.76, 8.16, 7.23, 13.13, 10.65, 10.79, 8.99, 18.62, 5.36]

branco= [13.99, 16.29, 17.82, 15.39, 18.92, 16.45, 52.63, 30.91, 28.34, 8.82, 29.68, 28.63, 24.56, 12.10, 15.46, 19.26, 14.58, 31.57, 35.40, 18.88, 14.78, 25.73, 34.76, 36.55, 20.51, 43.06, 14.66]

dados = pd.DataFrame({'negro':negro, 'branco':branco})

dados.corr()
print("\n\n\n")
print(dados.corr())
print("\n\n\n")
