import numpy as np

x = np.array([5.30, 5.25, 6.27, 7.42, 6.90, 6.62, 28.71, 11.33, 14.98, 3.03, 11.8, 13.35, 11.10, 4.31, 6.85, 7.43, 5.80, 12.42, 13.76, 8.16, 7.23, 13.13, 10.65, 10.79, 8.99, 18.62, 5.36]) # vetor com os valores de x
y = np.array([13.99, 16.29, 17.82, 15.39, 18.92, 16.45, 52.63, 30.91, 28.34, 8.82, 29.68, 28.63, 24.56, 12.10, 15.46, 19.26, 14.58, 31.57, 35.40, 18.88, 14.78, 25.73, 34.76, 36.55, 20.51, 43.06, 14.66]) # vetor com os valores de y

p1 = np.polyfit(x,y,1) # fornece os valores do intercepto e a inclinação

yfit = p1[0] * x + p1[1] # calcula os valores preditos
yresid = y - yfit # resíduo = valor real - valor ajustado (valor predito)
SQresid = sum(pow(yresid,2)) # soma dos quadrados dos resíduos 
SQtotal = len(y) * np.var(y) # número de elementos do vetor y vezes a variância de y
R2 = 1 - SQresid/SQtotal # coeficiente de determinação

print(p1) # imprime o intercepto e a inclinação
print(R2) # imprime coeficiente de determinação

import matplotlib.pyplot as plt

plt.plot(x,y,'o')
plt.plot(x,np.polyval(p1,x),'g--')
plt.xlabel("x")
plt.ylabel("y")
plt.show()
